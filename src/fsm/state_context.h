

/*
by bcc
2017.05.12
*/

#pragma once
#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxStateMachine.h"
#include "ofxSimpleTimer.h"
#include "ofxRemoteOfImage.h"

class ofxUserListener {
public:
	virtual ~ofxUserListener() {}
	virtual void stateHandler(std::string& arg) {}
};

class Context {
	
	ofxSimpleTimer delay;
public:
	void setup();
	void update();
	void exit();

	// system
	ofBaseApp* app;
	std::string currentState;
	std::string previousState;
	ofRectangle faceRect;
	ofPixels pixels;

	ofxRemoteOfImage server;
	ofImage servedImage;

	//make_face_image_
	int make_image_timeout;

	void changeState(std::string statename, int delaytime);
	void addListener(ofxUserListener* listener);
	void removeListener(ofxUserListener* listener);
	void stopTimer();
};
