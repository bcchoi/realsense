/*
by bcc
2017.05.12
*/

#pragma once
#include "state_context.h"

class ofxUserState : public itg::ofxState<Context>, ofxUserListener {
protected:
public:
	virtual string getName() { return ""; };
	virtual void stateEnter() {
		Context& ctx = getSharedData();
		ofLogNotice("current state") << getName();
		ctx.previousState = ctx.currentState;
		ctx.currentState = getName();
		ctx.addListener(this);
	}
	virtual void update() {
		Context& ctx = getSharedData();
		ctx.update();
	}
	virtual void stateExit() {
		Context& ctx = getSharedData();
		ctx.stopTimer();
		ctx.removeListener(this);
	}
	void stateHandler(std::string& state) {
		changeState(state);
	}
};
/*
ReadyFaceImage -> MakeFaceImage -> SendImage -> Idle
						^  				 |
						|		5sec	 |
						|________________|
*/
class ReadyFaceImage : public ofxUserState {
	ofApp* app;
public:
	string getName() { return "readyfaceimage"; }
	void setup() {
		ofLogNotice("loading") << getName();
		app = (ofApp*)getSharedData().app;
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		Context& ctx = getSharedData();
		ctx.make_image_timeout = ofGetElapsedTimeMillis();
	}
	void update() {
		changeState("makefaceimage");
	}
	void stateExit() {
		ofxOscMessage m;
		m.setAddress("/kairosfirebase");	// to
		m.addStringArg("/realsense");		// from
		m.addStringArg("faceimage");
		app->kairosFirebaseSender.sendMessage(m);
	}
};

class MakeFaceImage : public ofxUserState {
	ofApp* app;	
public:
	string getName() { return "makefaceimage"; }
	void setup() {
		ofLogNotice("loading") << getName();
		app = (ofApp*)getSharedData().app;
	}
	void update() {

		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - ctx.make_image_timeout > 5000) {
			changeState("idle");
			return;
		}
		if (app->realsense.getNumTrackedFaces() > 0) {			
			ctx.pixels = app->realsense.getColorPixelsRef();			
			ofRectangle rt(ctx.faceRect.x, ctx.faceRect.y, ctx.faceRect.width, ctx.faceRect.height);
			rt.scaleFromCenter(1.2f);
			ctx.pixels.crop(rt.x, rt.y, rt.width, rt.height);
			ctx.pixels.resize(rt.width*0.5, rt.height*0.5);

			changeState("sendimage");
		}
	}
};

/*
ReadyFullImage -> MakeFullImage -> SendImage -> Idle
						^  				 |
						|		1sec	 |
						|________________|
*/
class ReadyFullImage : public ofxUserState {

public:
	string getName() { return "readyfullimage"; }
	void setup() {
		ofLogNotice("loading") << getName();
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		Context& ctx = getSharedData();
		ctx.make_image_timeout = ofGetElapsedTimeMillis();
	}
	void update() {
		changeState("makefullimage");
	}
};
class MakeFullImage : public ofxUserState {
	ofApp* app;
public:
	string getName() { return "makefullimage"; }
	void setup() {
		ofLogNotice("loading") << getName();
		app = (ofApp*)getSharedData().app;
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		ofxOscMessage m;
		m.setAddress("/tensorflow");	// to
		m.addStringArg("/realsense");	// from
		app->tensorflowSender.sendMessage(m);
	}
	void update() {
		Context& ctx = getSharedData();
		if (ofGetElapsedTimeMillis() - ctx.make_image_timeout > 1000) {
			changeState("idle");
			return;
		}

		ctx.pixels = app->realsense.getColorPixelsRef();
		int w = FW / 3;
		int h = FH / 3;
		ctx.pixels.resize(w, h);
		changeState("sendimage");
	}
};

class SendImage : public ofxUserState {
public:
	string getName() { return "sendimage"; }
	void setup() {		
		ofLogNotice("loading") << getName();
	}	
	void update() {
		Context& ctx = getSharedData();
		ctx.server.begin();
		ofPixels pix;
		pix.allocate(ctx.pixels.getWidth(), ctx.pixels.getHeight(), OF_IMAGE_COLOR);
		ctx.servedImage.allocate(ctx.pixels.getWidth(), ctx.pixels.getHeight(), OF_IMAGE_COLOR);
		for (int y = 0; y < ctx.pixels.getHeight(); y++) {
			for (int x = 0; x < ctx.pixels.getWidth(); x++) {
				ofColor col = ctx.pixels.getColor(x, y);
				//col.r = 255 - col.r; 
				//col.g = 255 - col.g;
				//col.b = 255 - col.b;
				pix.setColor(x, y, col);
			}
		}

		memcpy(ctx.servedImage.getPixels(), pix, ctx.pixels.getWidth() * ctx.pixels.getHeight() * 3);
		ctx.servedImage.update(); //update the image after editing its piexls, to see results on screen

		ctx.server.end(); //call end() after editing the image, to avoid tearing when sending it to clients
		
		changeState(ctx.previousState);
	}
};

class Idle : public ofxUserState {	
public:
	string getName() { return "idle"; }
	void setup() {
		ofLogNotice("loading") << getName();		
	}
};

class FaceDetected : public ofxUserState {
	ofApp* app;	
	bool onlyOne;
public:
	string getName() { return "facedetected"; }
	void setup() {
		ofLogNotice("loading") << getName();		
		app = (ofApp*)getSharedData().app;		
	}
	void stateEnter() {
		ofxUserState::stateEnter();
		onlyOne = false;
	}
	void update() {
		ofxUserState::update();		
		if (onlyOne) return;
		
		Context& ctx = getSharedData();
		ofxOscMessage m;
		m.setAddress("/kairosfirebase");	// to
		m.addStringArg("/realsense");		// from
		m.addStringArg(getName());
		app->kairosFirebaseSender.sendMessage(m);		
		onlyOne = true;
		ctx.changeState("idle", 1000);
	}	
};

