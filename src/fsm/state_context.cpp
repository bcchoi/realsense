
/*
by bcc
2017.05.12
*/
#include "state_context.h"
#include "ofApp.h"

void Context::setup() {
	make_image_timeout = 0;
	currentState = "";
	previousState = "";
	server.setDesiredFramerate(5);
	server.setNetworkProtocol(REMOTE_OF_IMAGE_TCP);
	server.startServer(&servedImage);

}

void Context::exit() {
	server.stop();
}

void Context::changeState(std::string statename, int delaytime) {
	delay.setup(delaytime, statename);
	delay.start(false);
}
void Context::stopTimer() {
	delay.stop();
}
void Context::update() {
	delay.update();
}

void Context::addListener(ofxUserListener* listener) {
	ofAddListener(delay.TIMER_COMPLETE, listener, &ofxUserListener::stateHandler);
}
void Context::removeListener(ofxUserListener* listener) {
	ofRemoveListener(delay.TIMER_COMPLETE, listener, &ofxUserListener::stateHandler);
}
