#include "ofApp.h"
#include "fsm/states.h"

#define WG 60
#define HG 30
//--------------------------------------------------------------
void ofApp::setup(){
	ofSetVerticalSync(true);
	ofSetFrameRate(60);
	ofBackground(0);
	ofSetWindowPosition(640 + 60, 10);

	deviceSender.setup("localhost", DEVICEPORT);    	
	kairosFirebaseSender.setup("localhost", KAIROSFIREBASEPORT);

    receiver.setup(SELFPORT);
    
	realsense.setup(true, false, true, false, true); // grab color, and use textures
	realsense.open(); // opens device

	panPos = 0;	tiltPos = 0;

	stateMachine.getSharedData().app = this;
	stateMachine.getSharedData().setup();
	stateMachine.addState<MakeFaceImage>();
	stateMachine.addState<MakeFullImage>();
	stateMachine.addState<SendImage>();
	stateMachine.addState<ReadyFullImage>();
	stateMachine.addState<ReadyFaceImage>();
	stateMachine.addState<FaceDetected>();
	stateMachine.addState<Idle>();
	stateMachine.changeState("idle");
}

//--------------------------------------------------------------
void ofApp::update() {

	// regular event	
	while (receiver.hasWaitingMessages()) {
		ofxOscMessage m;
		receiver.getNextMessage(m);
		if (m.getAddress() == "/realsense") {
			if (m.getArgAsString(0) == "/kairosfirebase") {
				if (m.getArgAsString(1) == "readyfaceimage") {
					stateMachine.changeState("readyfaceimage");
				}
			}			
		}
	}
	realsense.update();
	std::vector<int> max;
	for (int i = 0; i < realsense.getNumTrackedFaces(); i++) {
		const ofxRSFace* face = realsense.getTrackedFace(i);
		const PXCRectI32& outR = face->getFaceRect();
		max.push_back(outR.w * outR.h);
	}

	std::vector<int>::iterator result;
	result = std::max_element(max.begin(), max.end());
	int at = std::distance(max.begin(), result);

	if (realsense.getNumTrackedFaces() > 0) {
		Context& ctx = stateMachine.getSharedData();
		if (ctx.currentState == "idle")
			stateMachine.changeState("facedetected");

		if (max.size() == 0) at = 0;
		const ofxRSFace* face = realsense.getTrackedFace(at);
		const PXCRectI32& outR = face->getFaceRect();
		ofPoint center;
		center.x = outR.x + (outR.w / 2);
		center.y = outR.y + (outR.h / 2);

		ctx.faceRect.set(outR.x, outR.y, outR.w, outR.h);

		if (ofGetElapsedTimeMillis() - lastUpdate > 100) {
			lastUpdate = ofGetElapsedTimeMillis();
			if (center.x > 0 && center.y > 0) {
				bool sended = false;
				if (center.x < (FW / 2) - (WG * 3)) {
					if (panPos < 50) {
						panPos += 1;
						sended = true;
					}
				}
				if (center.x > (FW / 2) + (WG * 3)) {
					if (panPos > -50) {
						panPos -= 1;
						sended = true;
					}
				}
				if (center.y < (FH / 2) - (HG * 3)) {
					if (tiltPos > -15) {
						tiltPos -= 1;
						sended = true;
					}
				}
				if (center.y > (FH / 2) + (HG * 3)) {

					if (tiltPos < 25) {
						tiltPos += 1;
						sended = true;
					}
				}

				if (sended) {
					sended = false;
					ofxOscMessage m;
					m.setAddress("/device");		// to
					m.addStringArg("/realsense");	// from
					m.addStringArg("motor");
					m.addIntArg(panPos);
					m.addIntArg(tiltPos);
					deviceSender.sendMessage(m);
					ofLogVerbose() << "pan: " << panPos << ", tilt: " << tiltPos;
				}
			}
		}
	}
}

//--------------------------------------------------------------
void ofApp::draw(){

	int w = FW / 3;
	int h = FH / 3;
	realsense.drawColor(0, 0, w, h); // top right
	
	std::vector<int> max;
	for (int i = 0; i < realsense.getNumTrackedFaces(); i++) {
		const ofxRSFace* face = realsense.getTrackedFace(i);
		const PXCRectI32& outR = face->getFaceRect();
		max.push_back(outR.w * outR.h);
	}

	std::vector<int>::iterator result;
	result = std::max_element(max.begin(), max.end());
	int at = std::distance(max.begin(), result);

	if (realsense.getNumTrackedFaces() > 0) {

		if (max.size() == 0) at = 0;	
		const ofxRSFace* face = realsense.getTrackedFace(at);
		const PXCRectI32& outR = face->getFaceRect();
		ofPoint center;
		center.x = outR.x + (outR.w / 2);
		center.y = outR.y + (outR.h / 2);

		ofPushStyle();
		ofNoFill();
		ofSetLineWidth(3);
		ofSetColor(ofColor::aqua);
		ofDrawCircle(center.x / 3, center.y / 3, 5);
		ofDrawRectangle(outR.x/3, outR.y/3, outR.w/3, outR.h/3);

		ofRectangle rt(outR.x / 3, outR.y / 3, outR.w / 3, outR.h / 3);
		rt.scaleFromCenter(1.2f);
		ofSetColor(ofColor::antiqueWhite);
		ofDrawRectangle(rt);
		ofPopStyle();
	}

	ofPushStyle(); 
		ofSetColor(127, 127, 255, 255);
		ofDrawLine(w / 2-WG, 0, w / 2-WG, h);
		ofDrawLine(w / 2+WG, 0, w / 2+WG, h);

		ofSetColor(255, 127, 255, 255);
		ofDrawLine(0, h / 2-HG, w, h / 2-HG);
		ofDrawLine(0, h / 2+HG, w, h / 2+HG);			

	ofPopStyle();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){	
	if (key == 'i') {
		stateMachine.changeState("init");		
	}	
}

void ofApp::exit() {
	realsense.close();
	stateMachine.getSharedData().exit();
}
