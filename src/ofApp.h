#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxNetwork.h"
#include "ofxRealSense.h"
//#include "ofxRemoteOfImage.h"
#include "fsm/state_context.h"

#define SELFPORT 12347
#define KAIROSFIREBASEPORT 12352
#define DEVICEPORT 12348
#define TENSORFLOWPORT 12351


#define FW 1920
#define FH 1080
class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		void exit();
		void keyPressed(int key);		
		
    ofxOscSender deviceSender;
	ofxOscSender tensorflowSender;
	ofxOscSender kairosFirebaseSender;
	ofxOscReceiver receiver;
    ofBuffer imageBuffer;

	ofxRealSense realsense;
	ofTexture colorInDepth, depthInColor; // for drawing calibrated color<->depth imgs

	//ofxRemoteOfImage server;
	//ofImage servedImage;
	ofPixels workspace;

	// dc motor
	int panPos;
	int tiltPos;
	int lastUpdate;


	// state
	itg::ofxStateMachine<Context> stateMachine;
};
